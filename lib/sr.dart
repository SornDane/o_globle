class SR {
  static const securityLoginId = 'securityLoginId';
  static const userId = 'userId';
  static const agencyId = 'agencyId';
  static const privateKey = 'privateKey';
  static const userFirebaseId = 'userFirebaseId';
  static const userType = 'userType';
  static const language = 'language';
  static const userEmail = 'userEmail';
  static const userPassword = 'userPassword';
  static const userName = 'userName';
  static const userPhone = 'userPhone';
  static const userAvatar = 'userAvatar';
  static const userPoint = 'userPoint';
  static const userSessionVersion = 'userSessionVersion';
}
import 'package:flutter/material.dart';
import 'package:o_globles/testing.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xff034DA2),
      ),
      home: TestScreen(),
    );
  }
}


import '../dataRow.dart';
import '../../g.dart' as gCo;
class DataForDropDown{
  String url = '${gCo.baseUrl}get-loan-approved?branch_id=1';
  String q = '';
  String where = '';
  CoDataRow dropDownDataRow = new CoDataRow();
  Future getData(setState, {bool refresh : false})async{
    String coUrl = await getURL(url);
    String coQ = await getQ(q);
    coQ = coQ != '' ? '&q=${coQ}' : '';
    String coWhere = await getWhere(where);
    //print('=========URL========${coUrl}');
    await dropDownDataRow.getDataRowSetState(
        setState,
        url: coUrl,
        refresh: refresh,
        where: '${coQ}${coWhere}',
        prepareData:(val) async {
          return await prepareDataDropDownDataRows(val);
        }, fields: {}
    );
  }
  Future processGetData(setState)async{
    await getData(setState, refresh: true).then((value){
      return value;
    });
  }
  Future<String> getURL(oldVal)async{
    return oldVal;
  }
  Future<String> getQ(oldVal)async{
    return oldVal;
  }
  Future<String> getWhere(oldVal)async{
    return oldVal;
  }
  prepareDataDropDownDataRows(rowList)async{
    Map row = {
      'list' : await dataRowsToDropdownData(dataList: rowList['rows_loan']['data']),
      'total' : rowList['rows_loan']['disbursement_number'],
    };
    return row;
  }
  dataRowsToDropdownData({required List dataList}){
    if(dataList != null){
      List _list= [];
      dataList.forEach((v) {
        _list.add({
          'id' : v['id'],
          'name' : v['title'],
        });
      });
      return _list;
    }
    return null;
  }
/// =========================================================================
}
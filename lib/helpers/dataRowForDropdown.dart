import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../g.dart' as gCo;
import 'dataRow.dart';
import '../g_widget.dart' as gWidget;

class DataRowForDropDown{
  Color backgroundColor = Colors.white;
  String url = '${gCo.baseUrl}get-branch';
  String q = '';
  String where = '';
  bool wrapIs = false;
  double wrapCrossAxisSpacing = 5;
  int wrapCrossAxisCount = 2;
  double wrapAspectRatioChild = 1;
  double wrapPaddingHorizontal = 15;
  double wrapPaddingVertical = 0;
  double addHeightModal = 0;
  late Widget headerWidget;
  late VoidCallback refreshModalData;
  CoDataRow dropDownDataRow = new CoDataRow();
  Map<String, dynamic> fields = "" as Map<String, dynamic>;

  void init(){

  }

  void afterInitRows(){

  }

  Future getData(setState, {bool refresh : false})async{
    String coUrl = await getURL(url);
    String coQ = await getQ(q);
    coQ = coQ != '' ? '&q=${coQ}' : '';
    String coWhere = await getWhere(where);

    Map<String, dynamic> coFields = await getFields(fields);

    //print('=========URL========${coUrl}');
    await dropDownDataRow.getDataRowSetState(
        setState,
        url: coUrl,
        refresh: refresh,
        fields: coFields,
        where: '${coQ}${coWhere}',
        prepareData:(val) async {
          return await prepareDataDropDownDataRows(val);
        }
    );
  }
  Future processGetData(setState)async{
    await getData(setState, refresh: true).then((value){
      return value;
    });
  }
  Future<String> getURL(oldVal)async{
    return oldVal;
  }
  Future<Map<String, dynamic>> getFields(oldVal)async{
    return oldVal;
  }
  Future<String> getQ(oldVal)async{
    return oldVal;
  }
  Future<String> getWhere(oldVal)async{
    return oldVal;
  }
  prepareDataDropDownDataRows(rowList)async{
    Map row = {
      'list' : await dataRowsToDropdownData(dataList: rowList['rows_branch']['data']),
      'total' : rowList['rows_branch']['total'],
    };
    return row;
  }
  dataRowsToDropdownData({required List dataList}){
    if(dataList != null){
      List _list= [];
      dataList.forEach((v) {
        _list.add({
          'id' : v['id'],
          'name' : v['title'],
        });
      });
      return _list;
    }
    return null;
  }
  Widget renderItems({required List list, required int index, required String id, required ValueChanged<String> onChanged}){
    return gWidget.renderItems(
      list: list,
      index: index,
      id: id,
      onChanged: onChanged
    );
  }
/// =========================================================================
}
import 'package:flutter/material.dart';

class VScopeVariable {
  late String id;
  dynamic coSetState;
  late BuildContext coContext;

  void refreshState(){
    if(coSetState != null){
      coSetState(() {
      });
    }
  }

  void initialize({@required dynamic setState, required BuildContext context, required String id}){
    this.coSetState = setState;
    this.coContext = context;
    this.id = id;
    this.firstState();
  }

  void firstState(){

  }
}
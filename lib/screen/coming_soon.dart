import 'package:flutter/material.dart';
import '../helper/stringExtension.dart';

class ComingSoon extends StatefulWidget {
  final String title;
  final String content;
  ComingSoon({required Key key, required this.title, this.content : 'Coming Soon'}) : super(key: key);

  @override
  _ComingSoonState createState() => _ComingSoonState();
}

class _ComingSoonState extends State<ComingSoon> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        title: Text('${widget.title == null ? 'coming_soon'.translate().toFirstUppercaseWord() : widget.title}'),
      ),
      body: Center(child: Container(
        color: Colors.white,
        child: Text('${widget.content  == null ? 'coming_soon'.translate().toFirstUppercaseWord() : widget.content}', style: TextStyle(fontSize: 25),),
      )),
    );
  }
}

import 'dart:async';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'dart:io';
import 'package:multi_image_picker/multi_image_picker.dart';
import '../g_form.dart';
import 'package:o_globles/g_widget.dart' as gWidget;
import 'package:o_globles/g.dart' as gCo;
import '../helper/stringExtension.dart';
/* ------------------------------------------------------------------------- */

class CoUploadMitiImage extends StatefulWidget {
  ValueChanged<List<File>> onChanged;
  String helperText;
  String title;
  List<String> imagesNetwork;
  ValueChanged<int> onDeletedImagesNetwork;
  CoAllowUploadImage coAllowUploadImage;
  List<File> files;
  bool onLoading;
  int maxImages;
  String actionBarColor;
  String actionBarTitleColor;
  bool lightStatusBar;
  String statusBarColor;
  bool startInAllView;
  String selectionLimitReachedText;
  bool selectedAssets;
  int maxSize;

  CoUploadMitiImage({
    required Key key,
    required this.files,
    this.onLoading : false,
    required this.helperText, required this.title,
    required this.onChanged,
    required this.imagesNetwork,
    required this.onDeletedImagesNetwork,
    this.coAllowUploadImage : CoAllowUploadImage.gallery,
    this.maxImages : 6,
    this.actionBarColor : "#034da2",
    this.actionBarTitleColor : "#FFFFFF",
    this.lightStatusBar : false,
    this.statusBarColor : '#034da2',
    this.startInAllView : true,
    required this.selectionLimitReachedText,
    this.selectedAssets : true,
    required this.maxSize

  })
      : super(key: key);

  @override
  _CoUploadMitiImageState createState() => _CoUploadMitiImageState();
}

class _CoUploadMitiImageState extends State<CoUploadMitiImage> {
  var field = new FieldProperty();
  late List<Asset> resultList;

  void getImageGallery() async {
    String error;
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: widget.maxImages,
        selectedAssets: resultList != null && widget.selectedAssets ? resultList : [],
        materialOptions: MaterialOptions(
          //actionBarTitle: "Action bar",
          //allViewTitle: "All view title",
          actionBarColor: widget.actionBarColor,
          actionBarTitleColor: widget.actionBarTitleColor,
          lightStatusBar: widget.lightStatusBar,
          statusBarColor: widget.statusBarColor,
          startInAllView: widget.startInAllView,
          // selectCircleStrokeColor: "#212121",
          selectionLimitReachedText: widget.selectionLimitReachedText,
        ),
      );

      setState(() {
      });
      pastFiles(resultList).then((value){
        setState(() {
          widget.files = value;
        });
        if(widget.onChanged != null){
          /*print('widget.onChanged(coFiles)====================================================================');
          print(value);*/
          widget.onChanged(value);
        }
      });


    } on Exception catch (e) {
      error = e.toString();
    }
  }

  Future<List<File>> pastFiles(List<Asset> resultList)async{
    List<File> coFiles = [];
    setState(() {
      widget.onLoading = true;
    });
    await Future.forEach(resultList, (Asset asset) async {
      File value = await gCo.getImageFileFromAsset(asset);
      if(widget.maxSize != null){
        value = await gCo.reSizeImageWith(file: value, maxSize: widget.maxSize);
      }
      /*print('value====================================================================');
      print(value);*/
      coFiles.add(value);
    });
    setState(() {
      widget.onLoading = false;
    });
    return coFiles;
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          inputDecorationTheme: InputDecorationTheme(
            border: field.fieldBorder,
            contentPadding: field.fieldContentPadding,
          )),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            widget.title != null ? Container(
              margin: EdgeInsets.only(bottom: 7),
              child: Text('${widget.title}', style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),),
            ) : gWidget.coZero(),
            Stack(
              children: [
                Container(
                  child: Column(
                    children: [
                      (widget.imagesNetwork == null ? false : (widget.imagesNetwork.length > 0)) ? Container(
                          margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
                          child: GridView.count(
                            shrinkWrap: true,
                            crossAxisCount: 3,
                            childAspectRatio: 1.5,
                            physics: NeverScrollableScrollPhysics(),
                            mainAxisSpacing: 8,
                            crossAxisSpacing: 8,
                            children: List.generate(widget.imagesNetwork == null ? 0 : widget.imagesNetwork.length, (index){
                              return Stack(
                                children: [
                                  Positioned(
                                    left: 0,
                                    right: 0,
                                    top: 0,
                                    bottom: 0,
                                    child: Container(
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(5),
                                        child: Container(
                                          color: Colors.grey[300],
                                          child: gWidget.imageUrlWidget(widget.imagesNetwork[index], fit: BoxFit.contain, errorImageAsset: ''),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      right: 3,
                                      top: 3,
                                      child: Container(
                                          height: 25,
                                          width: 25,
                                          decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10000),
                                              color: Colors.grey[400]
                                          ),
                                          child: InkWell(
                                              onTap: (){
                                                List<String> coImagesNetwork = widget.imagesNetwork;
                                                coImagesNetwork.removeAt(index);
                                                setState(() {
                                                  widget.imagesNetwork = coImagesNetwork;
                                                });
                                                if(widget.onDeletedImagesNetwork != null){
                                                  widget.onDeletedImagesNetwork(index);
                                                }
                                              },
                                              child: Icon(Icons.highlight_remove, color: Colors.red, size: 20,)
                                          )
                                      )
                                  )
                                ],
                              );
                            }),
                          )
                      ) : gWidget.coZero(),
                      InkWell(
                        onTap: () {
                          getImageGallery();
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: (widget.files == null ? false : (widget.files.length > 0)) ? 30 : 5),
                          constraints: BoxConstraints(
                              minHeight: 180, minWidth: double.infinity),
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 216, 214, 214),
                            borderRadius: new BorderRadius.circular(2.0),
                            border: new Border.all(
                              width: 1.0,
                              color: Colors.black38,
                            ),
                          ),
                          /// Loop
                          child: (widget.files == null ? false : (widget.files.length > 0)) ? GridView.count(
                            shrinkWrap: true,
                            crossAxisCount: 3,
                            childAspectRatio: 1.5,
                            physics: NeverScrollableScrollPhysics(),
                            mainAxisSpacing: 8,
                            crossAxisSpacing: 8,
                            children: List.generate(widget.files == null ? 0 : widget.files.length, (index){
                              return ClipRRect(
                                borderRadius: BorderRadius.circular(5),
                                child: Container(
                                  color: Colors.grey[400],
                                  child: Image.file(widget.files[index]),
                                ),
                              );
                            }),
                          ) : Center(
                            child: SizedBox.fromSize(
                              size: Size(150, 56), // button width and height
                              child: //ClipOval(
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.photo_library), // icon
                                    SizedBox(width: 5),
                                    Text(
                                      "add_images".translate().toFirstUppercaseWord(),
                                      style: TextStyle(color: Colors.black87),
                                    ), // text
                                  ],
                                ),
                              ),
                              //),
                            ),
                          ),
                          /// end loop
                        ),
                      ),
                    ],
                  ),
                ),
                widget.onLoading ?
                    Positioned(
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0,
                        child: Container(
                          color: Colors.grey.withOpacity(0.5),
                          child: Center(
                            child: Container(
                                height: 40,
                                width: 40,
                                child: CircularProgressIndicator()
                            ),
                          ),
                        )
                    ) :
                    gWidget.coZero()
              ],
            ),
            widget.helperText != null
                ? Container(
              margin: EdgeInsets.only(top: 5, left: 20),
              child: Text(
                '${widget.helperText}',
                style: TextStyle(color: Colors.black54, fontSize: 11),
              ),
            )
                : gWidget.coZero()
          ],
        ),
      ),
    );
  }
}
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:o_globles/form/textField.dart';
import 'package:o_globles/g_form.dart' as gForm;
import 'package:o_globles/g_widget.dart' as gWidget;
import 'package:o_globles/g.dart' as gCo;

typedef CoTimeWidgetBuilder = Widget Function(
    BuildContext context, String val);

class CoTime extends StatefulWidget {
  late String text;
  late String helperText;
  late FormFieldValidator<String> validator;
  late ValueChanged<TimeOfDay> onChanged;
  late TimeOfDay initialTime;
  TextEditingController controller = TextEditingController();
  late bool disable;
  late CoTimeWidgetBuilder customBuilder;

  @override
  _CoDateState createState() => _CoDateState();
}

class _CoDateState extends State<CoTime> {
  DateTime dateNow = new DateTime.now();
  TimeOfDay times = new TimeOfDay.now();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget showDatePicker() {
    return CupertinoDatePicker(
      mode: CupertinoDatePickerMode.time,
      initialDateTime: new DateTime(dateNow.year, dateNow.month, dateNow.day, times.hour, times.minute,),
      //initialDateTime: widget.initialTime == null ? dateNow : new DateTime(dateNow.year, dateNow.month, dateNow.day, widget.initialTime.hour, widget.initialTime.minute, ),
      onDateTimeChanged: (newDateTime) {
        times = TimeOfDay(hour: newDateTime.hour, minute: newDateTime.minute);
      },
    );
  }

  void popUp(){
    if(widget.disable == false){
      times = new TimeOfDay(hour: widget.initialTime.hour, minute: widget.initialTime.minute);
      gWidget.coModalBottomSheet(context, text: widget.text, scrollAble: false ,withOK: true, build: showDatePicker(),
          onOK: (){
            widget.initialTime = new TimeOfDay(hour: times.hour, minute: times.minute);
            setState(() {
            });
            Navigator.of(context, rootNavigator: true).pop();
            if(widget.onChanged != null){
              widget.onChanged(times);
            }
          }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if(widget.initialTime == null){
      widget.initialTime = new TimeOfDay(hour: times.hour, minute: times.minute);
    } else {
      times = new TimeOfDay(hour: widget.initialTime.hour, minute: widget.initialTime.minute);
    }
    return widget.customBuilder == null ? coTextField(
      disable: widget.disable,
      text: widget.text,
      readOnly: true,
      validator: widget.validator,
      controller: TextEditingController(text: '${gCo.formatTimeOfDay(widget.initialTime == null ? times : widget.initialTime)}'),
      onTap: (){
        popUp();
      }, onChanged: (String value) {  }, helperText: '', onFieldSubmitted: (String value) {  },
    ) :
    InkWell(
      onTap: (){
        popUp();
      },
      child: widget.customBuilder(context, '${gCo.formatTimeOfDay(widget.initialTime == null ? times : widget.initialTime)}')
    );
  }
}

import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:o_globles/widget/scroll.dart';
import '../g_widget.dart' as gWidget;
import '../g_form.dart' as gForm;
import '../g_form.dart';
import '../helper/stringExtension.dart';
/* ------------------------------------------------------------------------- */

class coStepStyle2 extends StatefulWidget {
  List<CoStepModel> build;
  int index;
  ValueChanged<int> onBack;
  ValueChanged<int> onNext;
  //ValueChanged<int> next;
  bool validate;
  ValueChanged<int> onDone;
  bool validation;
  String text;
  GestureTapCallback next;
  List<dynamic> buttonTextList;
  bool loading;
  bool appBarBrightnessDark;
  coStepStyle2(
      {required this.build,
      this.index = 1,
      this.validation: false,
      this.validate: true,
      required this.onBack,
      required this.text,
      required this.onNext,
      required this.next,
      required this.buttonTextList,
      required this.loading,
      this.appBarBrightnessDark: true,
      required this.onDone});

  @override
  _coStepStyle2State createState() => _coStepStyle2State();
}

enum transitionType {
  In,
  Out,
}

class _coStepStyle2State extends State<coStepStyle2>
    with SingleTickerProviderStateMixin {
  late Animation<Offset> animation;
  late AnimationController animationController;
  Duration duration = Duration(milliseconds: 800);
  transitionType transition = transitionType.In;
  int oldVal = 1;
  bool onReady = false;

  @override
  void initState() {
    super.initState();
    onReady = true;
    animationController = AnimationController(
      vsync: this,
      duration: duration,
    );
    animation = Tween<Offset>(
      begin: Offset(transition == transitionType.Out ? 0.0 : 1.0, 0.0),
      end: Offset(transition == transitionType.Out ? -1.0 : 0.0, 0.0),
    ).animate(CurvedAnimation(
      parent: animationController,
      curve: Curves.fastLinearToSlowEaseIn,
    ));
    Future<void>.delayed(Duration(milliseconds: 1), () {
      animationController.forward();
      //animationController.dispose();
    });
  }

  Future CoAnimation({symbol: "+"}) async {

      if (widget.validation == false) {
        animationController.reverse();
        Future<void>.delayed(Duration(milliseconds: 810), () {
          animationController.forward();
          setState(() {
            widget.index = widget.index + (symbol == '+' ? 1 : -1);
          });
        });
      } else {
        if (widget.onNext != null && symbol == '+') {
          animationController.reverse();
          Future<void>.delayed(Duration(milliseconds: 810), () {
            animationController.forward();
            setState(() {
              widget.onNext(widget.index + 1);
            });
          });
        }
        if (widget.onBack != null && symbol == '-') {
          animationController.reverse();
          Future<void>.delayed(Duration(milliseconds: 810), () {
            animationController.forward();
            setState(() {
              widget.onBack(widget.index - 1);
            });
          });
        }
      }

  }

  @override
  void dispose() {
    // Don't forget to dispose the animation controller on class destruction
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.build != null) {
      return Scaffold(
            appBar: AppBar(
              brightness: widget.appBarBrightnessDark ? Brightness.dark : Brightness.light,
              elevation: 0.5,
              leading: new IconButton(
                icon: new Icon(Icons.arrow_back,
                    color: Colors.white),
                onPressed: () {
                  setState(() {
                    if (widget.index <= 1) {
                      Navigator.pop(context);
                    }
                    CoAnimation(symbol: '-');
//                    if (widget.validation == false) {
//                      widget.index = widget.index - 1;
//                    } else {
//                      if (widget.onBack != null) {
//                        widget.onBack(widget.index - 1);
//                      }
//                    }
                  });
                },
              ),
              backgroundColor: Theme.of(context).primaryColor,
              title: widget.index <= 1
                  ? Text(
                      widget.text,
                      style: TextStyle(color: Colors.white),
                    )
                  : Text(
                      widget.text,
                      style: TextStyle(color: Colors.white),
                    ),
            ),
            floatingActionButton: !widget.loading ? Container(
              margin: EdgeInsets.only(left: 22),
              padding: EdgeInsets.only(left: 5),
              width: MediaQuery.of(context).size.width,
              height: 80,
              child: Column(
                children: <Widget>[
                  Container(
                      child: widget.index != widget.build.length
                          ? coButton(
                              height: 40,
                              text: '${widget.buttonTextList != null ? (widget.buttonTextList.length + 1 >= widget.index + 1 ? (widget.buttonTextList[widget.index - 1] != null ? widget.buttonTextList[widget.index - 1] : 'next'.translate().toUpperCase()) : 'next'.translate().toUpperCase()) : 'next'.translate().toUpperCase()}',
                              onPressed: () {
                                CoAnimation();
                              }, icon: Icon(Icons.description), color:Colors.grey,

                      )
                          : coButton(
                              height: 40,
                              text: '${widget.buttonTextList != null ? (widget.buttonTextList.length + 1 >= widget.index + 1 ? (widget.buttonTextList[widget.index - 1] != null ? widget.buttonTextList[widget.index - 1] : 'done'.translate().toUpperCase()) : 'done'.translate().toUpperCase()) : 'done'.translate().toUpperCase()}',
                              onPressed: () {
                                setState(() {
                                  if (widget.onDone != null) {
                                    widget.onDone(widget.index + 1);
                                  }
                                });
                              },
                icon: Icon(Icons.description), color:Colors.grey)),
                  SizedBox(
                    height: 10,
                  ),
                  stepBar(),
                ],
              ),
            ) : gWidget.coZero(),
            body: !widget.loading ? CoSlideTransition(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: List.generate(widget.build.length, (index) {
                          return Visibility(
                              visible:
                                  widget.index == (index + 1) ? true : false,
                              child: Expanded(
                                  child: Container(
                                margin: EdgeInsets.only(top: 10, bottom: 100),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                        child: Container(
                                      padding:
                                          EdgeInsets.only(right: 0, left: 0),
                                      child: CoScroll(
                                        onRefresh: (String value) {  },
                                        scrollController: ScrollController(),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            widget.build[index].header != null ? widget.build[index].header : SizedBox(width: 0, height: 0,),
                                            widget.build[index].title != null ?
                                            Container(
                                              margin: EdgeInsets.only(left: 15, top: 6),
                                              child: Text(widget.build[index].title, style: TextStyle(
                                                  color: Colors.black87,
                                                  fontSize: 20,
                                                  fontWeight:
                                                  FontWeight.w600)),
                                            )
                                                : SizedBox(width: 0, height: 0,),
                                            widget.build[index].title != null ? Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              padding: EdgeInsets.only(
                                                  top: 10,
                                                  bottom: 10,
                                                  left: 15),
                                              margin:
                                                  EdgeInsets.only(bottom: 5),
                                              child: widget.build[index].headLine,
                                            ) : SizedBox(width: 0, height: 0,),
                                            Container(
                                              height: 10,
                                            ),
                                            widget.build[index].body,
                                          ],
                                        ),
                                      ),
                                    )),
                                  ],
                                ),
                              )));
                        }),
                      ),
                    ),
                  )
                ],
              ),
            ) :
            Container(
              height: 100,
              child: Center(
                child: Container(
                    height: 30,
                    width: 30,
                    child: CircularProgressIndicator()
                ),
              ),
            ),
          );
    } else {
      return Container(
            color: Colors.transparent,
          );
    }
  }

  Widget CoSlideTransition({child}) {
    if (animation == null) {
      return child;
    } else {
      return SlideTransition(
        position: animation,
        child: child,
      );
    }
  }

  Widget stepBar() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 7),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: List.generate(widget.build.length, (index) {
            return Expanded(
              flex: 1,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: widget.index >= (index + 1)
                            ? Theme.of(context).accentColor
                            : Colors.black38,
                      ),
                      height: 15,
                    ),
                    /*Divider(
                      color: widget.index >= (index + 1)
                          ? Theme.of(context).accentColor
                          : Colors.black45,
                      thickness: 10,
                    ),*/
                  ),
                  widget.build.length > index + 1
                      ? SizedBox(
                          width: 8,
                        )
                      : SizedBox(
                          width: 0,
                        ),
                ],
              ),
            );
          })),
    );
  }
}


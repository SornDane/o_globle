import 'package:flutter/material.dart';

class FormTabScrollModel {
  String id;
  String title;
  String key;
  GlobalKey keyContentItem;
  GlobalKey keyTabItem;
  Widget body;
  FormTabScrollModel(
      {required this.id, required this.title, required this.key, required this.keyContentItem, required this.keyTabItem, required this.body});
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['key'] = this.key;
    data['keyContentItem'] = this.keyContentItem;
    data['keyTabItem'] = this.keyTabItem;
    data['body'] = this.body;
    return data;
  }
}

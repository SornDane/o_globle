import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:o_globles/form/textField.dart';
import '../g_form.dart' as gForm;
import '../g_widget.dart' as gWidget;
import '../g.dart' as gCo;
import '../g_model.dart';
import '../helper/stringExtension.dart';


typedef CoRangeDateWidgetBuilder = Widget Function(
    BuildContext context, String val);
class CoRangeDate extends StatefulWidget {
  String text;
  String helperText;
  FormFieldValidator<String> validator;
  ValueChanged<CoRangeDateModel> onChanged;
  CoRangeDateModel initialDateTime;
  CoRangeDateWidgetBuilder customBuilder;
  TextEditingController controller = TextEditingController();
  bool disable;
  String dateFormatDisplay;

  CoRangeDate(
      {required Key key,
        required this.text,
        required this.helperText,
        required this.onChanged,
        required this.controller,
        required this.validator,
        this.disable: false,
        required this.initialDateTime,
        required this.customBuilder,
        this.dateFormatDisplay : 'd MMM, y',
      })
      : super(key: key);
  @override
  _CoRangeDateState createState() => _CoRangeDateState();
}

class _CoRangeDateState extends State<CoRangeDate> {
  var now = new DateTime.now();
  CoRangeDateModel dateTimes = CoRangeDateModel(start: DateTime.now(), end: DateTime.now());

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget showDatePicker() {
    return Container(
      child: StatefulBuilder(
          builder: (context2, setState2) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).hintColor.withOpacity(0.15),
                          offset: Offset(0, 3),
                          blurRadius: 10)
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                topLeft: Radius.circular(10),
                              )),
                          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          child: Text(
                            'from_date'.translate().toFirstUppercaseWord() + ':',
                            style:
                            TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                          )),
                      Divider(
                        color: Colors.black26,
                        height: 1,
                      ),
                      Container(
                        height: 250,
                        child: CupertinoDatePicker(
                          mode: CupertinoDatePickerMode.date,
                          initialDateTime: dateTimes.start,
                          onDateTimeChanged: (newDateTime) {
                            dateTimes.start = newDateTime;
                            if(gCo.dateToInt(dateTimes.start) > gCo.dateToInt(dateTimes.end)){
                              dateTimes.end = dateTimes.start;
                            }
                            setState2(() {
                            });
                            /*widget.initialDateTime = dateTimesNew;
                            setState(() {
                            });

                            if(widget.onChanged != null){
                              widget.onChanged(dateTimes);
                            }*/
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context).hintColor.withOpacity(0.15),
                          offset: Offset(0, 3),
                          blurRadius: 10)
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                topLeft: Radius.circular(10),
                              )),
                          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                          child: Text(
                            'to_date'.translate().toFirstUppercaseWord() + ':',
                            style:
                            TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
                          )),
                      Divider(
                        color: Colors.black26,
                        height: 1,
                      ),
                      Container(
                        height: 250,
                        child: CupertinoDatePicker(
                          mode: CupertinoDatePickerMode.date,
                          minimumDate: new DateTime(dateTimes.start.year, dateTimes.start.month, dateTimes.start.day),
                          initialDateTime: dateTimes.end,
                          onDateTimeChanged: (newDateTime) {
                            dateTimes.end = newDateTime;
                            setState2(() {
                            });
                            /*widget.initialDateTime = dateTimes;
                          setState(() {
                          });

                          if(widget.onChanged != null){
                            widget.onChanged(dateTimes);
                          }*/
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ],
            );
          }
      ),
    );
  }

  void popUp(){
    if(widget.disable == false){
      dateTimes = new CoRangeDateModel(start: widget.initialDateTime.start, end: widget.initialDateTime.end);
      gWidget.coModalBottomSheet(context, text: widget.text, scrollAble: true, fullScreen: true, withOK: true, build: showDatePicker(),
          onOK: (){
            widget.initialDateTime = new CoRangeDateModel(start: dateTimes.start, end: dateTimes.end);
            setState(() {
            });
            Navigator.of(context, rootNavigator: true).pop();
            if(widget.onChanged != null){
              widget.onChanged(dateTimes);
            }
          }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if(widget.initialDateTime == null){
      widget.initialDateTime = new CoRangeDateModel(start: dateTimes.start, end: dateTimes.end);
    } else {
      dateTimes = new CoRangeDateModel(start: widget.initialDateTime.start, end: widget.initialDateTime.end);
    }
    /// customer builder co
    return widget.customBuilder == null ? coTextField(
      disable: widget.disable,
      text: widget.text,
      readOnly: true,
      validator: widget.validator,
      controller: TextEditingController(text: '${gCo.formatDateOfDay((widget.initialDateTime == null ? true : (widget.initialDateTime.start == null) ) ? dateTimes.start : widget.initialDateTime.start, format: widget.dateFormatDisplay)} - ${gCo.formatDateOfDay((widget.initialDateTime == null ? true : (widget.initialDateTime.end == null) ) ? dateTimes.end : widget.initialDateTime.end, format: widget.dateFormatDisplay)}'),
      onTap: (){
        popUp();
      }, helperText: '', onChanged: (String value) {  }, onFieldSubmitted: (String value) {  },
    ) : InkWell(
      onTap: (){
        popUp();
      },
      child: widget.customBuilder(context, '${gCo.formatDateOfDay((widget.initialDateTime == null ? true : (widget.initialDateTime.start == null) ) ? dateTimes.start : widget.initialDateTime.start, format: widget.dateFormatDisplay)} - ${gCo.formatDateOfDay((widget.initialDateTime == null ? true : (widget.initialDateTime.end == null) ) ? dateTimes.end : widget.initialDateTime.end, format: widget.dateFormatDisplay)}'),
    );
  }
}

import 'dart:async';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import '../g.dart';
import '../g_form.dart';
import '../g_widget.dart' as gWidget;
import '../g.dart' as gCo;
import '../helper/stringExtension.dart';
/* ------------------------------------------------------------------------- */

enum CoAllowUploadImage{gallery, camera, both}

class coUploadImage extends StatefulWidget {
  ValueChanged<File> onChanged;
  String helperText;
  String title;
  String imageNetwork;
  CoAllowUploadImage coAllowUploadImage;
  File file;
  BoxFit fit;
  bool blur;
  bool onLoading;
  int maxSize;

  coUploadImage({required Key key,
    required this.file,
    this.fit: BoxFit.contain,
    this.blur : true,
    this.onLoading : false,
    required this.helperText,
    required this.title,
    required this.onChanged,
    required this.imageNetwork,
    this.coAllowUploadImage : CoAllowUploadImage.gallery,
    required this.maxSize
  })
      : super(key: key);

  @override
  _coUploadImageState createState() => _coUploadImageState();
}

class _coUploadImageState extends State<coUploadImage> {
  var field = new FieldProperty();

  Future getImageGallery() async {
    try {
      File image = (await ImagePicker().pickImage(source: ImageSource.gallery)) as File;

      if(widget.maxSize != null  && image != null){
        setState(() {
          widget.onLoading = true;
        });
        await gCo.reSizeImageWith(file: image, maxSize: widget.maxSize).then((value){
          image = value;
        });
        setState(() {
          widget.onLoading = false;
        });
      }
      if (widget.onChanged != null) {
        widget.onChanged(image);
      }
      setState(() {
        widget.file = image;
      });
    } on Exception catch (e) {

    }

  }

  Future getImageCamera() async {
    try {
      File image = (await ImagePicker().pickImage(source:  ImageSource.camera)) as File;

      if(widget.maxSize != null && image != null){
        setState(() {
          widget.onLoading = true;
        });
        await gCo.reSizeImageWith(file: image, maxSize: widget.maxSize).then((value){
          image = value;
        });
        setState(() {
          widget.onLoading = false;
        });
      }
      if (widget.onChanged != null) {
        widget.onChanged(image);
      }
      setState(() {
        widget.file = image;
      });
    } on Exception catch (e) {

    }

  }

  Future getImageBoth() async {
    coAction(context, actions: <Widget>[
      CupertinoActionSheetAction(
          child: Text("from_gallery".translate().toFirstUppercaseWord()),
          isDefaultAction: true,
          onPressed: () {
            Navigator.of(context, rootNavigator: true).pop();
            getImageGallery();
          }),
      CupertinoActionSheetAction(
          child: Text("from_camera".translate().toFirstUppercaseWord()),
          isDestructiveAction: true,
          onPressed: () {
            Navigator.of(context, rootNavigator: true).pop();
            getImageCamera();
          })
    ], title: '');
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
          inputDecorationTheme: InputDecorationTheme(
            border: field.fieldBorder,
            contentPadding: field.fieldContentPadding,
          )),
      child: InkWell(
        onTap: () {
          if (widget.coAllowUploadImage == CoAllowUploadImage.camera) {
            getImageCamera();
          } else if(widget.coAllowUploadImage == CoAllowUploadImage.both){
            getImageBoth();
          } else {
            getImageGallery();
          }
        },
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              widget.title != null ? Container(
                margin: EdgeInsets.only(bottom: 7),
                child: Text('${widget.title}', style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),),
              ) : gWidget.coZero(),
              Container(
                height: 180,
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 216, 214, 214),
                  borderRadius: new BorderRadius.circular(2.0),
                  border: new Border.all(
                    width: 1.0,
                    color: Colors.black38,
                  ),
                ),
                child: Stack(
                  children: <Widget>[
                    widget.blur ? ClipRect(
                      child: new Container(
                        height: 180,
                        decoration: new BoxDecoration(
                       /*   image: widget.file != null && widget.onLoading == false || (widget.imageNetwork != null && widget.imageNetwork != '')
                              ? DecorationImage(
                              image: widget.file != null
                                  ? FileImage(widget.file)
                                  : NetworkImage(widget.imageNetwork),
                              fit: BoxFit.cover)
                              : null,*/
                        ),
                        child: new BackdropFilter(
                          filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                          child: new Container(
                            decoration: new BoxDecoration(color: Colors.white.withOpacity(0.1)),
                          ),
                        ),
                      ),
                    ) : Container(height: 180,),
                    widget.onLoading == false ? Positioned(
                      top: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        height: 180,
                        decoration: new BoxDecoration(
                         
                        ),
                        child: InkWell(
                          splashColor: Colors.transparent, // splash color
                          child: widget.file == null && widget.imageNetwork == null
                              ? Center(
                            child: SizedBox.fromSize(
                              size: Size(150, 56), // button width and height
                              child: //ClipOval(
                              Material(
                                color: Colors.transparent,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.image), // icon
                                    SizedBox(width: 5),
                                    Text(
                                      "add_image".translate().toFirstUppercaseWord(),
                                      style: TextStyle(color: Colors.black87),
                                    ), // text
                                  ],
                                ),
                              ),
                              //),
                            ),
                          )
                              : Container(
                            margin: EdgeInsets.fromLTRB(10, 10, 5, 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Icon(
                                  Icons.image,
                                  color: Colors.white,
                                ),
                                Container(
                                    margin: EdgeInsets.only(top: 2, left: 3),
                                    child: Text(
                                      'edit'.translate().toUpperCase(),
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 14),
                                    ))
                              ],
                            ),
                          ),
                        ),
                      ),
                    ) : gWidget.coZero(),
                    widget.onLoading ? Positioned(
                      top: 75,
                      left: 0,
                      right: 0,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ) : SizedBox(width: 0, height: 0,),
                  ],
                ),
              ),
              widget.helperText != null
                  ? Container(
                margin: EdgeInsets.only(top: 5, left: 20),
                child: Text(
                  '${widget.helperText}',
                  style: TextStyle(color: Colors.black54, fontSize: 11),
                ),
              )
                  : gWidget.coZero()
            ],
          ),
        ),
      ),
    );
  }
}
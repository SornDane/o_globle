import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class coMultiDropdownModalBottomSheetWidget extends StatefulWidget {
  List list;
  List selectedId;
  ValueChanged<List> onChanged;

  coMultiDropdownModalBottomSheetWidget(
      {
        required this.list,
        required this.onChanged,
        required this.selectedId,
       })
      ;

  @override
  _coMultiDropdownModalBottomSheetWidgetStateState createState() => _coMultiDropdownModalBottomSheetWidgetStateState();
}

class _coMultiDropdownModalBottomSheetWidgetStateState extends State<coMultiDropdownModalBottomSheetWidget> {
  ItemScrollController _scrollController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener = ItemPositionsListener.create();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      widget.selectedId = widget.selectedId;
    });
    WidgetsBinding.instance!.addPostFrameCallback((_) => _onLoading(context));
  }

  void _onLoading(BuildContext context) async {

  }

  @override
  Widget build(BuildContext context) {
    return CupertinoScrollbar(
      child: ScrollablePositionedList.builder(
          physics: BouncingScrollPhysics(),
          itemScrollController: _scrollController,
          itemPositionsListener: itemPositionsListener,
          itemCount: widget.list != null ? widget.list.length : 0,
          itemBuilder: (context, index) {
            return CheckboxListTile(
              title: Text(
                '${widget.list[index]['name']}',
                style: TextStyle(color: Colors.black87),
              ),
              value: widget.selectedId != null ? widget.selectedId.contains('${widget.list[index]['id'].toString()}') : false,
              onChanged: (value) {
                //print(value);
                if (value == true) {
                  setState(() {
                    if(widget.selectedId == null){
                      widget.selectedId = [];
                    }
                    widget.selectedId.add('${widget.list[index]['id'].toString()}');
                  });
                } else {
                  setState(() {
                    widget.selectedId.remove('${widget.list[index]['id'].toString()}');
                  });
                }
                if (widget.onChanged != null) {
                  widget.onChanged(widget.selectedId);
                }
              },
            );
          }),
    );
  }

}
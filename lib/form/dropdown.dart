import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:o_globles/ui_icons.dart';
import '../g_widget.dart';
import '../helpers/defaultDataRowForDropDown/coDataRow.dart';
import '../g_form.dart';
import '../helpers/dataRowForDropdown.dart';
import 'widget/widget.dart';
import '../helper/stringExtension.dart';

typedef CoDropdownWidgetBuilder = Widget Function(
    BuildContext context, String val, bool onLoading);

class CoDropdown extends StatefulWidget {
  FocusNode focusNode;
  List list;
  Map selectedMap;
  String text;
  var name;
  coFormSize formSize;
  int selectedIndex;
  String selectedId;
  bool returnIndex;
  bool onLoading;
  ValueChanged<String> onSearchWithApi;
  String helperText;
  var icon;
  DataRowForDropDown dataRow;
  Map<String, dynamic> dataRowSearchFields;
  DataForDropDown dataRows;
  Future prepareData;
  bool autoGetDataRow;
  bool noSelectItemRow;
  bool disable;
  bool disableSearch;
  bool styleOther;
  FormFieldValidator<String> validator;

  //GestureTapCallback onTap;
  ValueChanged<String> onChanged;
  ValueChanged<List> onDataRowList;
  TextEditingController textEditingController = TextEditingController();
  CoDropdownWidgetBuilder customBuilder;

  CoDropdown(
      {required Key key,
      required this.list,
      required this.text,
      required this.textEditingController,
      required this.onChanged,
      this.selectedIndex: 0,
      required this.validator,
      required this.selectedId,
      this.returnIndex: false,
      this.onLoading: false,
      required this.onSearchWithApi,
      required this.helperText,
      required this.prepareData,
      this.icon,
      required this.dataRow,
      required this.onDataRowList,
      this.autoGetDataRow : true,
      this.noSelectItemRow : false,
      this.formSize : coFormSize.normal,
      this.disable: false,
      this.disableSearch: false,
      this.styleOther: false,
      required this.dataRows,
      required this.dataRowSearchFields,
      required this.selectedMap,
      required this.customBuilder,
      required this.focusNode,
    })
      : super(key: key);
  @override
  _CoDropdownState createState() => _CoDropdownState();
}

class _CoDropdownState extends State<CoDropdown> {
  var field = new FieldProperty();
  var searchController = new TextEditingController();
  var values;
  FocusNode _focusNode = new FocusNode();
  var id;
  var name;

  late PersistentBottomSheetController _controller; // <------ Instance variable
  final _scaffoldKey =
      GlobalKey<ScaffoldState>(); // <---- Another instance variable

  FocusNode get _effectiveFocusNode =>
      widget.focusNode ?? (_focusNode ??= FocusNode());

  @override
  void initState(){
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) => _onLoading(context));
    _focusNode.addListener(_focusNodeReRender);
    // TODO: implement initState
    if(widget.dataRow != null){
      widget.dataRow.init();
    }
    setState(() {});
//    var getIdValue;
//    if(widget.selectedId!=null){
//      var getId = widget.list.indexWhere((row) => row["id"] == widget.selectedId);
//      if(getId != null && getId != -1){
//        print(getId);
//        getIdValue = getId;
//        setState(() {
//          widget.selectedIndex = getIdValue != null ? getIdValue :  widget.selectedIndex;
//        });
//      }
//    }
  }

  _focusNodeReRender() {
    setState(() {
      // Re-renders
    });
  }

  void _onLoading(BuildContext context) async {
    /*print('_onLoading ${id}');
    if(widget.onChanged != null && widget.list != null && widget.selectedId == null){
      if(widget.list.length > 0){
        widget.onChanged(widget.list[0]['id']);
      }
    }*/
    if(widget.dataRow != null && (widget.list == null ? true : (widget.list.length > 0 ? false : true))){
      widget.dataRow.q = '';
      searchController = TextEditingController(text: '');
      if(widget.autoGetDataRow){
        getDataRow();
      }

    }
  }

  void getDataRow(){
    widget.dataRow.fields = widget.dataRowSearchFields;
    widget.dataRow.getData(setState, refresh: true).then((value) async {
      if(widget.onDataRowList != null){
        List cList = widget.noSelectItemRow ? await addNoSelect(widget.dataRow.dropDownDataRow.list) : widget.dataRow.dropDownDataRow.list;
        widget.dataRow.dropDownDataRow.list = cList;
        widget.list = cList;
        widget.onDataRowList(cList);
        widget.dataRow.afterInitRows();
      }
      /*//this.setState(() {
        widget.list = widget.dataRow.dropDownDataRow.list;

        //});
        setState(() {
          widget.list = widget.dataRow.dropDownDataRow.list;
        });*/
      //print('===============onLoading Data==============${widget.list}');
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    /*print('=========id=====${id.toString()}');
    if(widget.dataRow != null){
      print('=========widget.list=====${widget.list}');
    }*/
    if (widget.list != null) {
      if (widget.list.length > 0) {
        //id = widget.list[widget.selectedIndex]['id'];
        if (widget.selectedId != null) {
          id = widget.selectedId;
          final idValue = widget.list
              .indexWhere((row) => row["id"].toString() == id.toString());
          //print('idValue : ${idValue}');
          if (idValue != -1) {
            widget.selectedIndex = idValue;
            widget.selectedIndex = widget.selectedIndex;
            name = widget.list[widget.selectedIndex]['name'];
            id = id;
            widget.textEditingController = TextEditingController(text: name);
          }
          //print(widget.selectedIndex);

        } else {
          /*id = widget.list[0]['id'];
          widget.selectedIndex = widget.selectedIndex;
          name = widget.list[widget.selectedIndex]['name'];
          id = id;
          widget.textEditingController = TextEditingController(text: name);*/
        }
        //_onLoading(context);

      }
    }
    if(widget.selectedId == null || widget.selectedId == 'null'){
      name = noSelectItemRow['name'];
      widget.textEditingController = TextEditingController(text: noSelectItemRow['name']);
    }
    /*else {
      name = null;
      widget.textEditingController = TextEditingController(text: '');
    }*/
    if(widget.selectedMap!= null && widget.selectedId!=null){
      int index = widget.list.indexWhere((v) => v['id'].toString() == widget.selectedId.toString());
      if(index == -1){
        if(widget.selectedMap['id'].toString() == widget.selectedId.toString()){
          widget.list.insert(0, {
            'id' : widget.selectedMap['id'].toString(),
            'name' : widget.selectedMap['name'].toString(),
          });
        }
      }
    }
    if(widget.styleOther && widget.dataRow == null){
      return FormField<String>(
          validator: widget.validator,
          builder: (FormFieldState<String> state) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new InputDecorator(
                    decoration: InputDecoration(
                      labelText: widget.text,
                      contentPadding: new EdgeInsets.only(left: 0, top: 39.0, bottom: 0),
                      helperText: null,
                      border: field.fieldBorder.copyWith(borderSide: const BorderSide(color: Colors.transparent, width: 1.5),),
                      enabledBorder: field.fieldBorder.copyWith(borderSide: const BorderSide(color: Colors.transparent, width: 1.5),),
                      focusedBorder: field.fieldBorder.copyWith(borderSide: const BorderSide(color: Colors.transparent, width: 1.5),),
                      labelStyle: _focusNode.hasFocus
                          ? field.fieldLabelHasFocusStyle
                          .copyWith(fontWeight: FontWeight.w400, fontSize: 20)
                          : field.fieldLabelHasFocusStyle
                          .copyWith(color: Colors.black54, fontWeight: FontWeight.w400, fontSize: 20),

                      //field.fieldLabelHasFocusStyle.copyWith(color: Colors.black54,fontWeight: FontWeight.w400)
                    ),
                    isHovering: true,
                    isFocused: _effectiveFocusNode.hasFocus,
                    child: Wrap(
                      children: List.generate(widget.list != null ? widget.list.length : 0, (index) {
                        bool isSelected = widget.selectedId != null ? (widget.selectedId.toString() == widget.list[index]['id'].toString()) : false;
                        return InkWell(
                          onTap: (){
                            if(!widget.disable){
                              widget.selectedId = widget.list[index]['id'].toString();
                              if (widget.onChanged != null) {
                                if (widget.returnIndex == false) {
                                  //print('on change ${widget.list[index]['id']}');
                                  //print('${value}');
                                  final idVal = widget.list[index]['id'].toString();
                                  //print("idVal : ${idVal}");
                                  widget.onChanged('${idVal}');
                                } else {
                                  widget.onChanged(index.toString());
                                }
                              }
                              setState(() {
                              });
                            }
                          },
                          child: Container(
                              margin: EdgeInsets.only(right: 8, bottom: 0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: isSelected ? Colors.blueAccent : Colors.white,
                                border: Border.all(width: 1, color: isSelected ? Colors.blueAccent : Colors.black87)
                                //border: Border.all(width: 2, color: Colors.blueAccent)
                              ),
                              child: Container(
                                  padding: EdgeInsets.only(
                                      right: 15, bottom: 10, top: 10, left: 15),
                                  child: Text(
                                    '${widget.list[index]['name']}',
                                    style: TextStyle(color: isSelected ? Colors.white : Colors.black87, fontSize: 17),
                                  ))),
                        );
                          //widget.selectedId != null ? widget.selectedId.toString() == widget.list[index]['id'].toString()
                      }),
                    )
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Text(state.hasError? state.errorText! : "",
                   // state.hasError ? state.errorText : '',
                    style:
                    TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
                  ),
                ),
              ],
            );
          }
      );
    }
    return widget.customBuilder == null ? Container(
      child: TextFormField(
        validator: widget.validator,
        focusNode: _focusNode,
        controller: widget.textEditingController,
        style: field.fieldStyle,
        onTap: () {
          if(widget.disable == false && !widget.onLoading){
            if(widget.dataRow != null ? widget.dataRow.dropDownDataRow.onSearching : false){
              // code
            } else {
                coModalBottomSheet(context);
            }

          }
        },
        readOnly: true,
        decoration: InputDecoration(
          fillColor:  Colors.grey[300],
          filled: widget.disable,
          prefixIcon: widget.icon != null ? Icon(widget.icon) : null,
          contentPadding: widget.formSize == coFormSize.normal ? field.fieldContentPadding : field.fieldContentPaddingSmall,
          helperText: widget.helperText,
          border: field.fieldBorder,
          enabledBorder: field.fieldEnabledBorder,
          focusedBorder: field.fieldBorder,
          labelText: ("${widget.text}"),
          labelStyle: _focusNode.hasFocus
              ? field.fieldLabelHasFocusStyle
              : name != null && name != ''
                  ? field.fieldLabelHasFocusStyle
                      .copyWith(color: Colors.black54)
                  : field.fieldLabelStyle,
          suffixIcon: (widget.dataRow != null ? widget.dataRow.dropDownDataRow.onSearching : false) || widget.onLoading ?
          Container(
              child: CupertinoActivityIndicator()
          )
          : const Icon(
            Icons.keyboard_arrow_down,
            color: Colors.black87,
          ),
          //labelStyle: TextStyle(color: Colors.red, fontSize: 20),
        ),
      ),
    ) :
    InkWell(
        onTap: () {
          if(widget.disable == false && !widget.onLoading){
            if(widget.dataRow != null ? widget.dataRow.dropDownDataRow.onSearching : false){
              // code
            } else {
              coModalBottomSheet(context);
            }
          }
        },
        child: widget.customBuilder(context, name.toString(), (widget.dataRow != null ? widget.dataRow.dropDownDataRow.onSearching : false) || widget.onLoading)
    );
  }

  void coModalBottomSheet(context) async {
    showCupertinoModalPopup(
        //isScrollControlled: true,
        //isDismissible: true,
        context: context,
        builder: (BuildContext bc) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState2) {
                void searchDataRow({bool refresh : true})async{
                  widget.dataRow.fields = widget.dataRowSearchFields;
                  setState2((){
                    widget.dataRow.q = '${searchController.text}';
                  });
                  await widget.dataRow.getData(setState2, refresh: refresh);
                  //print('=============================${widget.dataRow.dropDownDataRow.list}');
                  setState2(() {
                    widget.list = widget.dataRow.dropDownDataRow.list;
                    if(widget.onDataRowList != null){
                      widget.onDataRowList(widget.dataRow.dropDownDataRow.list);
                    }
                  });
                }
                double keybordHight = MediaQuery.of(context).viewInsets.bottom;

                if(widget.dataRow != null){
                  widget.dataRow.refreshModalData = searchDataRow;
                }

                return Container(
                  margin: EdgeInsets.only(bottom: keybordHight > 0.0 ? keybordHight : 0.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(10.0),
                          topRight: const Radius.circular(10.0))),
                  height: keybordHight > 0.0 ? MediaQuery.of(context).size.height - (keybordHight + 40) : (MediaQuery.of(context).size.height / 2 + (widget.dataRow != null && widget.disableSearch == false ? 200 : 0)) + (widget.dataRow == null ? 0 : widget.dataRow.addHeightModal),
                  child: Material(
                    color: Colors.transparent,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: IconButton(
                                  icon: Icon(Icons.close),
                                  onPressed: () {
                                    Navigator.of(context).pop(); // closing
                                  }),
                            ),
                            Expanded(
                              flex: 5,
                              child: Center(
                                child: Text(
                                  '${widget.text}',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w500,
                                    fontSize: widget.formSize == coFormSize.small ? 16.0 : 18.0,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Divider(
                          color: Colors.black26,
                        ),
                        widget.dataRow != null && widget.disableSearch == false ? Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 15),
                              child: coTextField(
                                controller: searchController,
                                formSize: widget.formSize,
                                text: 'search'.translate().toFirstUppercase(),
                                textInputAction: TextInputAction.search,
                                onFieldSubmitted: (val){
                                  searchDataRow();
                                },
                                onChanged: (val){
                                  setState2(() {
                                    // refresh
                                  });
                                }, helperText: '', onTap: () {  }, validator: (String? value) {  },
                               /* suffixIcon: Container(
                                  width: searchController.text != '' ? 98 : 50,
                                  child: Row(
                                    children: [
                                      searchController.text != '' ? IconButton(
                                          icon: Icon(Icons.clear, color: Colors.black54,),
                                          onPressed: () {
                                            setState2(() {
                                              searchController = TextEditingController(text: '');
                                            });
                                          }) : coZero(),
                                      IconButton(
                                          icon: Icon(Icons.search),
                                          onPressed: () {
                                            searchDataRow();
                                          }),
                                    ],
                                  ),
                                ),*/
                              ),

                            ),
                            SizedBox(height: 5,),
                            Divider(
                              color: Colors.black12,
                              height: 2,
                            ),
                          ],
                        ) : SizedBox(width: 0, height: 0,),
                        if(widget.dataRow == null ? false : (widget.dataRow.headerWidget != null))
                          widget.dataRow.headerWidget,
                        widget.dataRow != null ?
                          widget.dataRow.dropDownDataRow.onSearching ?
                          Container(
                            width: MediaQuery.of(context).size.width,
                            color: widget.dataRow!= null ? ((widget.dataRow.dropDownDataRow.list == null ? true : (widget.list.length > 0 ? false : true)) ? null : widget.dataRow.backgroundColor) : Colors.white,
                            child: Center(
                              child: Container(
                                margin: EdgeInsets.only(top: 10, bottom: 10),
                                  child: CircularProgressIndicator()
                              ),
                            ),
                          )
                              : SizedBox(width: 0,) : SizedBox(width: 0,),
                        Expanded(
                          child: widget.list!= null ? widget.list.length > 0 ?
                          Container(
                            color: widget.dataRow!= null ? widget.dataRow.backgroundColor : Colors.white,
                            child: coModalBottomSheetWidget(
                              list: widget.list,
                              selectedId: widget.selectedId,
                              onChanged: widget.onChanged,
                              id: id.toString(),
                              selectedIndex: widget.selectedIndex,
                              returnIndex: widget.returnIndex,
                              dataRow: widget.dataRow,
                              noSelectItemRow: widget.noSelectItemRow,
                              onLoadMore: (val){
                                if(widget.dataRow != null){
                                  //print('More');
                                  searchDataRow(refresh: false);
                                }
                              },
                            ),
                          ): Container(
                            height: 50,
                            child: Center(
                                child: Text('no_item'.translate().toFirstUppercase())
                            ),
                          ) : Container(
                            height: 50,
                            child: Center(
                                child: Text('no_item'.translate().toFirstUppercase()
                              ),
                            ),
                          ),
                        ),
                        /*widget.dataRow != null ?
                        widget.dataRow.dropDownDataRow.onLoading ?
                        Container(
                            margin: EdgeInsets.only(top: 10, bottom: 10),
                            child: CircularProgressIndicator()
                        )
                            : SizedBox(width: 0,) : SizedBox(width: 0,),*/
                        /*:
                          Container(
                              margin: EdgeInsets.only(top: 20),
                              child: CircularProgressIndicator()
                          ),*/
                      ],
                    ),
                  ),
                );
              },
            );

        });
  }
}



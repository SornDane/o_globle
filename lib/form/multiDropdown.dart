import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../g_form.dart';
import 'widget/coMultiDropdownModalBottomSheetWidget.dart';
class coMutiDropdown extends StatefulWidget {
  FocusNode focusNode;
  List list;
  String text;
  List selectedIds;
  ValueChanged<List> onChanged;
  FormFieldValidator<String> validator;

  coMutiDropdown(
      {required Key key, required this.list, this.text : 'Group', required this.focusNode, required this.onChanged, required this.selectedIds, required this.validator})
      : super(key: key);

  @override
  _coMutiDropdownState createState() => _coMutiDropdownState();
}

class _coMutiDropdownState extends State<coMutiDropdown> {
  var field = new FieldProperty();
  var values;
  FocusNode _focusNode = new FocusNode();

  FocusNode get _effectiveFocusNode =>
      widget.focusNode ?? (_focusNode ??= FocusNode());

  @override
  void initState() {
    super.initState();
    _focusNode = new FocusNode();

    // listen to focus changes
    _focusNode.addListener(
        () => print('focusNode updated: hasFocus: ${_focusNode.hasFocus}'));
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print('close');
  }

  void setFocus() {
    FocusScope.of(context).requestFocus(_focusNode);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        setFocus();
        //print(widget.selectedIds);
        if(widget.list != null){
          if(widget.list.length > 0){
            coModalBottomSheet(context);
          }
        }

      },
      child: FormField<String>(
        validator: widget.validator,
        builder: (FormFieldState<String> state) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new InputDecorator(
                decoration: InputDecoration(
                  labelText: widget.text,
                  contentPadding: field.fieldContentPadding,
                  helperText: null,
                  border: field.fieldBorder,
                  enabledBorder: state.hasError ? field.fieldEnabledBorderError : field.fieldEnabledBorder,
                  focusedBorder: field.fieldBorder,
                  labelStyle: _focusNode.hasFocus
                      ? field.fieldLabelHasFocusStyle
                          .copyWith(fontWeight: FontWeight.w400)
                      : field.fieldLabelHasFocusStyle
                          .copyWith(color: Colors.black54, fontWeight: FontWeight.w400),
                  suffixIcon: const Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.black87,
                  ),

                  //field.fieldLabelHasFocusStyle.copyWith(color: Colors.black54,fontWeight: FontWeight.w400)
                ),
                isHovering: true,
                isFocused: _effectiveFocusNode.hasFocus,
                child: Wrap(
                  children: List.generate(widget.list != null ? widget.list.length : 0, (index) {
                    return widget.selectedIds != null ? widget.selectedIds.contains(widget.list[index]['id'].toString()) == true ? Container(
                        margin: EdgeInsets.only(right: 7, bottom: 7),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.blueAccent,
                          //border: Border.all(width: 2, color: Colors.blueAccent)
                        ),
                        child: Container(
                            padding: EdgeInsets.only(
                                right: 10, bottom: 5, top: 5, left: 10),
                            child: Text(
                              '${widget.list[index]['name']}',
                              style: TextStyle(color: Colors.white, fontSize: 15),
                            ))) : SizedBox(height: 20,): SizedBox(height: 20);
                  }),
                )
              ),
              SizedBox(height: 5.0),
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Text(
                  state.hasError ? state.errorText! : '',
                  style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
                ),
              ),
            ],
          );
        }
      ),
    );
  }

  void coModalBottomSheet(context) async {
    showCupertinoModalPopup(
        //isScrollControlled: true,
        //isDismissible: true,
        context: context,
        builder: (BuildContext bc) {
          return Container(
            width: MediaQuery.of(context).size.width,
            decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(10.0),
                    topRight: const Radius.circular(10.0))),
            height: MediaQuery.of(context).size.height / 2,
            child: Material(
              color: Colors.transparent,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: IconButton(
                            icon: Icon(Icons.close),
                            onPressed: () {
                              Navigator.of(context).pop(); // closing
                            }),
                      ),
                      Expanded(
                        flex: 4,
                        child: Center(
                          child: Text(
                            '${widget.text}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              fontSize: 18.0,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: InkWell(
                          onTap: (){
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            margin: EdgeInsets.only(right: 0),
                            child: Text(
                              'OK',
                              style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.black26,
                  ),
                  Expanded(
                    child: coMultiDropdownModalBottomSheetWidget(
                      list: widget.list,
                      selectedId: widget.selectedIds,
                      onChanged: (val){
                        if(widget.onChanged != null){
                          widget.onChanged(val);
                          setState(() {
                            widget.selectedIds = val;
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}

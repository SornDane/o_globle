import 'package:flutter/material.dart';
import '../g_model.dart';
import '../g.dart' as gCo;
class CoBottomNavigationBar extends StatefulWidget {
  int currentTap;
  Color primaryColor;
  Color color;
  Color borderColor;
  Color backgroundColor;
  Color notiBackgroundColor;
  Color notiColor;
  ValueChanged<int> onChange;
  bool disableText;
  List<CoBottomNavigationBarModel> tapBuild;
  CoBottomNavigationBar({this.currentTap : 1, required this.tapBuild, required this.primaryColor, required this.color, required this.borderColor, required this.backgroundColor, required this.onChange, this.disableText : false, required this.notiBackgroundColor, required this.notiColor});
  @override
  _BottomNavigationBarState createState() => _BottomNavigationBarState();
}

class _BottomNavigationBarState extends State<CoBottomNavigationBar> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(widget.primaryColor == null){
      widget.primaryColor = gCo.mainColor;
    }
    if(widget.color == null){
      widget.color = Colors.grey;
    }
    if(widget.borderColor == null){
      widget.borderColor = Colors.grey;
    }
    if(widget.notiBackgroundColor == null){
      widget.notiBackgroundColor = Colors.red;
    }
    if(widget.notiColor == null){
      widget.notiColor = Colors.white;
    }
    if(widget.backgroundColor == null){
      widget.backgroundColor = Colors.white;
    }
    return widget.tapBuild != null ? Container(
      color: widget.backgroundColor,
      height: !widget.disableText ? 60 : 55,
      //padding: EdgeInsets.symmetric(horizontal: 20),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: List.generate(widget.tapBuild.length, (index){
            CoBottomNavigationBarModel row = widget.tapBuild[index];
            return LayoutBuilder(
              builder: (context, size) {
                return Material(
                  color: Colors.white,
                  child: InkWell(
                    onTap: (){
                      if(widget.onChange != null){
                        widget.onChange(index + 1);
                      }
                    },
                    child: Stack(
                      children: [
                        Container(
                          //padding: EdgeInsets.only(left: 10, right: 10),
                          width: MediaQuery.of(context).size.width / widget.tapBuild.length,
                          decoration: BoxDecoration(
                              border: Border(
                                  top: BorderSide(color: widget.currentTap == (index + 1) ? widget.primaryColor : widget.borderColor, width: widget.currentTap == (index + 1) ? 2 : 1)
                              )
                          ),
                          child: Column(
                            children: [
                              SizedBox(height: 5,),
                              Icon(widget.tapBuild[index].icon, size: 26, color: widget.currentTap == (index + 1) ? widget.primaryColor : widget.color,),
                              SizedBox(height: 2,),
                              !widget.disableText ?
                                Text  (widget.tapBuild[index].title, style: TextStyle(fontSize: 12, fontWeight: widget.currentTap == (index + 1) ? FontWeight.bold : FontWeight.normal, color: widget.currentTap == (index + 1) ? widget.primaryColor : widget.color),)
                              : SizedBox(width: 0, height: 0,)
                            ],
                          ),
                        ),
                        if(row.nofi != null)
                          Positioned(
                              right: 20,
                              top: 0,
                              child: Container(
                                decoration: BoxDecoration(
                                  color: widget.notiBackgroundColor,
                                  borderRadius: BorderRadius.circular(1000)
                                ),
                                padding: EdgeInsets.symmetric(horizontal: 9, vertical: 5),
                                child: Text(row.nofi, style: TextStyle(color: widget.notiColor, fontSize: 12),),
                              )
                          )
                      ],
                    ),
                  ),
                );
              }
            );
          })
      ),
    ) : Container(color: Colors.transparent,);
  }
}

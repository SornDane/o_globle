import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:o_globles/widget/scroll.dart';
import '../g_widget.dart' as gWidget;
import '../helper/stringExtension.dart';

class CoModalBottomSheet extends StatefulWidget {
  Widget build;
  String text;
  bool withOK;
  bool fullScreen;
  GestureTapCallback onOK;
  bool scrollAble;
  Color backgroundColor;
  CoModalBottomSheet({required Key key, required this.text, required this.build, this.withOK : false, this.fullScreen : false, required this.onOK, this.scrollAble : true, this.backgroundColor: Colors.white}) : super(key : key);
  @override
  _CoModalBottomSheetState createState() => _CoModalBottomSheetState();
}

class _CoModalBottomSheetState extends State<CoModalBottomSheet> {
  @override
  void initState() {
    // TODO: implement initState
    print('======OK=====');
    coModalBottomSheet(context,
      text: widget.text,
      build: widget.build,
      withOK: widget.withOK,
      fullScreen: widget.fullScreen,
      onOK: widget.onOK,
      scrollAble: widget.scrollAble,
    );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.transparent,);
  }
}

void coModalBottomSheet(context, {String text : 'Bottom Sheet', required Widget build, bool withOK : false, bool fullScreen : false, required GestureTapCallback onOK, bool scrollAble : true, Color backgroundColor: Colors.white}) async {
  showCupertinoModalPopup(
    //useRootNavigator: false,
    //semanticsDismissible: true,
    //isScrollControlled: true,
    //isDismissible: true,
      context: context,
      builder: (BuildContext bc) {
          return Container(
            width: MediaQuery.of(context).size.width,
            decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(10.0),
                    topRight: const Radius.circular(10.0))),
            height: fullScreen ? MediaQuery.of(context).size.height - 56 : MediaQuery.of(context).size.height / 2,
            child: Material(
              color: Colors.transparent,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: IconButton(
                            icon: Icon(Icons.close),
                            onPressed: () {
                              Navigator.of(context, rootNavigator: true).pop();
                            }),
                      ),
                      Expanded(
                        flex: 5,
                        child: Center(
                          child: Text(
                            '${text}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              fontSize: 18.0,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: withOK == true ? InkWell(
                          onTap: onOK == null ? (){
                            Navigator.of(context, rootNavigator: true).pop();
                          } : onOK,
                          child: Container(
                            margin: EdgeInsets.only(right: 0),
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                            child: Text(
                              'ok'.translate().toUpperCase(),
                              style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        ) : Container(
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.black26,
                    height: 1,
                  ),
                  Expanded(
                    child: Container(
                        color: backgroundColor,
                        child: build != null ? ( scrollAble ?CoScroll(
                        onRefresh: (String value) {},
                        scrollController: ScrollController(),
                        child: build) : build) : SizedBox(width: 0, height: 0,)
                    ),
                  ),
                ],
              ),
            ),
          );
      });
}
import 'package:flutter/material.dart';

Widget renderItems({required List list, required int index, required String id, required ValueChanged<String> onChanged}){
  return RadioListTile(
    title: Text(
      '${list[index]['name']}',
      style: TextStyle(color: Colors.black87),
    ),
    value: list[index]['id'].toString(),
    groupValue: id.toString(), onChanged: (String? value) {  },

  );
}
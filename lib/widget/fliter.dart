import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoFilter extends StatefulWidget {
  String title;
  Color color;
  IconData icon;
  GestureTapCallback onTap;
  String value;
  bool onLoading;
  Widget widgetRight;
  CoFilter({required Key key, required this.title, required this.color, required this.onTap, required this.icon, required this.value, this.onLoading : false, required this.widgetRight}) : super(key : key);
  @override
  _CoFilterState createState() => _CoFilterState();
}

class _CoFilterState extends State<CoFilter> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.black45
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(widget.title, style: TextStyle(fontSize: 14 ,color: Colors.white, fontWeight: FontWeight.bold),),
            SizedBox(width: 5,),
            widget.value != null ? Container(
                margin: EdgeInsets.only(right: 5),
                child: Text(': ${widget.value}', style: TextStyle(fontSize: 14 ,color: Colors.white),)
            ) : SizedBox(width: 0, height: 0,),
            widget.onLoading ? Container(
                margin: EdgeInsets.only(left: 5),
                width: 15,
                height: 15,
                child: CircularProgressIndicator()
            ) : (widget.widgetRight == null ?
            Icon(widget.icon != null ? widget.icon : Icons.arrow_drop_down, size: 16, color: Colors.white)
                : widget.widgetRight)
          ],
        ),
      ),
    );
  }
}

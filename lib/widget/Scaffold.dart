import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../g.dart' as gCo;

class CoScaffold extends StatefulWidget {
  Widget body;
  String title;
  double titleFontSize;
  String subtitle;
  Color color;
  GlobalKey<ScaffoldState> scaffoldKey;
  bool home;
  List<Widget> action;
  Widget floatingActionButton;
  Widget drawer;
  Widget endDrawer;
  Widget leading;
  CoScaffold({required Key key, required this.body, required this.title, required this.color,required this.subtitle, required this.scaffoldKey, this.home : false, required this.action, required this.floatingActionButton, this.titleFontSize : 23, required this.drawer, required this.endDrawer, required this.leading}) : super(key : key);

  @override
  _CoScaffoldState createState() => _CoScaffoldState();
}

class _CoScaffoldState extends State<CoScaffold> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Stack(
      children: <Widget>[
        Container(
          width: double.infinity,
          height: double.infinity,
          child: _header(context),
        ),
        Positioned(
            bottom: -10,
            left: -130,
            child: _circularContainer(width * .7, Colors.transparent,
                borderColor: Colors.lightBlueAccent.withOpacity(0.3))),
        Scaffold(
          key: widget.scaffoldKey,
          backgroundColor: Colors.transparent,
          drawer: widget.drawer,
          endDrawer: widget.endDrawer,
          appBar: AppBar(
            automaticallyImplyLeading: true,
            brightness: Brightness.dark,
            backgroundColor: Colors.transparent,
            leading: widget.leading,
            elevation: 0,
            title: Column(
              children: <Widget>[
                Text(
                  '${widget.title}',
                 style: TextStyle(fontSize: widget.titleFontSize),
                ),
                SizedBox(height: 2,),
                widget.subtitle != null ? Text('${widget.subtitle}',style: TextStyle(fontSize: 14),): SizedBox(height: 0, width: 0,),
              ],
            ),
            actions: widget.action,
          ),
          body: widget.body != null ? widget.body : SizedBox(height: 0, width: 0,),
          floatingActionButton: widget.floatingActionButton,
        ),
      ],
    );
  }
  Widget _header(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      color: widget.color != null ? widget.color : Colors.white,
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10), bottomRight: Radius.circular(50)),
            child: Container(
                height: 200,
                width: width,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                ),
                child: Stack(
                  fit: StackFit.expand,
                  alignment: Alignment.center,
                  children: <Widget>[
                    widget.home ? SizedBox(width: 0, height: 0) : Positioned(
                        top: 30,
                        right: -100,
                        child: _circularContainer(300, Colors.blueGrey)),
                   /* Positioned(
                        top: -100,
                        left: -45,
                        child: _circularContainer(width * .5, Colors.deepPurple)),*/
                    Positioned(
                        top: -180,
                        right: -30,
                        child: _circularContainer(width * .7, Colors.transparent,
                            borderColor: Colors.white38)),
                    Positioned(
                        top: 40,
                        left: 0,
                        child: Container(
                            width: width,
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(height: 10),
                                SizedBox(height: 20),
                              ],
                            )))
                  ],
                )),
          ),
        ],
      ),
    );
  }
  Widget _circularContainer(double height, Color color,
      {Color borderColor = Colors.transparent, double borderWidth = 2}) {
    return Container(
      height: height,
      width: height,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
        border: Border.all(color: borderColor, width: borderWidth),
      ),
    );
  }
}

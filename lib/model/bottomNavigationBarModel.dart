import 'package:flutter/material.dart';

class CoBottomNavigationBarModel{
  IconData icon;
  String title;
  String nofi;
  CoBottomNavigationBarModel({this.icon : Icons.home_outlined, this.title : 'Home', required this.nofi});
}
class DefaultModel {
  int id;
  String name;
  String image;


  DefaultModel({required this.id, required this.name, required this.image});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}

import 'package:o_globles/g.dart' as gCo;

String tran(String key){
  if(gCo.coAppLanguage == null ? false : (!gCo.checkKeyMap(gCo.coAppLanguage, key) ? false : (!gCo.checkKeyMap(gCo.coAppLanguage[key], gCo.lang) ? false : (gCo.coAppLanguage[key][gCo.lang] != null && gCo.coAppLanguage[key][gCo.lang] != '')))){
    return gCo.coAppLanguage[key][gCo.lang];
  }
  return _convertLang(key);
}

String _convertLang(key){
  if(key!= null && key != ''){
    return key.replaceAll('_', ' ');
  }
  return key;
}